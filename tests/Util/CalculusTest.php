<?php

namespace App\Tests\Util;

use PHPUnit\Framework\TestCase;
use App\Util\Calculus;

class CalculusTest extends TestCase
{
    private $calculus;

    public function setUp() {
        $this->calculus = new Calculus();
    }
    /* On indique ici avec l'annotation le dataProvider qu'utilisera
    la méthode. On indique ensuite à la méthode en question le nom
    des arguments qui seront fournis par le dataProvider.
    */
    /**
     * @dataProvider successProvider
     */
    public function testResultSuccess($a,$b,$operator,$expect) {
        $result = $this->calculus->result($a,$b,$operator);
        

        $this->assertEquals($expect, $result);
        
    }

    /**
     * @expectedException Exception
     */
    public function testResultTypError() {
        
        $this->calculus->result("bloup", "blip", "+");
    }
    
    /**
     * On va utiliser cette méthode pour fournir à un test un
     * jeu de données pour pouvoir réaliser rapidement plusieurs
     * tests. Chaque tableau du tableau représente un jeu de données.
     * Chaque élément d'un des tableau représente un argument qui pourra
     * être passé à une méthode de test
     */
    public function successProvider() {
        return [
            [1, 1, "+", 2],
            [-4, 1, "+", -3],
            [1.2, 1.1, "+", 2.3],
            [1, 1, "-", 0],
            [1, 1, "*", 1],
            [4, 2, "/", 2]
        ];
    }
}
