<?php

namespace App\Tests\Util;

use PHPUnit\Framework\TestCase;
use App\Util\Greeter;

/**
 * Les test unitaires servent à tester les composants d'une application
 * de manière isolée les uns des autres, dans un environnement contrôlé.
 * Ces tests seront relancés très souvent afin de voir si les 
 * modifications qu'on fait nous font régresser ou pas. 
 */
class GreeterTest extends TestCase {
    private $greeter;
    /**
     * Cette méthode sera lancée avant chacun des tests, elle permet
     * de mettre en place l'environnement de test pour qu'il soit
     * le même entre chacun des tests.
     */
    public function setUp() {
        $this->greeter = new Greeter();
    }

    public function testFirst() {
        $this->assertEquals("bloup", "bloup");
        $this->assertFalse(false);
    }

    public function testGreetSuccess() {
        //On chope le résultat de la méthode à tester
        $result = $this->greeter->greet("bloup");
        //Ici, on indique le résultat qu'on attend
        $expect = "Hello bloup, how's it going ?";
        //On vérifie si le résultat attendu correspond au résultat obtenu
        $this->assertEquals($expect, $result);
    }

    public function testGreetNumber() {
        $result = $this->greeter->greet(45);
        $expect = "Hello 45, how's it going ?";
        $this->assertEquals($expect, $result);
    }
    /*
    On peut tester des méthodes qui déclenchent des erreurs dans
    certains cas en utilisant l'annotation expectedException suivi
    du type d'erreur attendue. Ici, quand on appel la méthode greet
    avec un tableau en argument, on s'attend à avoir une TypeError
    exception.
    */
    /**
     * @expectedException TypeError
     */
    public function testGreetTypeException() {
        $this->greeter->greet([]);
    }

}