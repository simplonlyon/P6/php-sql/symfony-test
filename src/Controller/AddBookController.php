<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\BookType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Book;

class AddBookController extends Controller
{
    /**
     * @Route("/add/book", name="add_book")
     */
    public function index(Request $request)
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();
        }

        return $this->render('add_book/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
